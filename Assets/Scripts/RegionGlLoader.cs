﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

public class RegionGlLoader : MonoBehaviour
{
    // Start is called before the first frame update
    public string fileName;
    public string visit;
    public Transform RegionParent;
    private Region r;
    public Vector3 min = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    public Vector3 max = new Vector3(-Mathf.Infinity, -Mathf.Infinity, -Mathf.Infinity);
    public float Diameter;
    public Vector3 center;
    public Material lineMaterial;
    void Start()
    {
        Debug.Log("Loading Region: " + fileName);
        r = readFile();
        instantiateRegion(r);
    }

    Region readFile()
    {
        Debug.Log(Application.streamingAssetsPath);
        string filePath = Application.streamingAssetsPath + "/" + fileName;
        return JsonUtility.FromJson<Region>(File.ReadAllText(filePath));
    }
    
    void instantiateRegion(Region r)
    {
        Debug.Log("Instantiate");
        // Apply the line material
        lineMaterial.SetPass(0);

        //GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        //GL.MultMatrix(transform.localToWorldMatrix);

        // Draw lines

        var k = 0; 
        for (int i = 0; i < 2; i++)
        {
            GL.PushMatrix();
            GL.MultMatrix(transform.localToWorldMatrix);
            //GL.Begin(GL.LINE_STRIP);
            GL.Begin(GL.LINES);
            GL.Color(Color.red);
            var points = new Vector3[((int)r.lineLengths[i])];
            for (int j = (int)r.lineOffsets[i];
                j < (int)r.lineLengths[i] + (int)r.lineOffsets[i]; j++)
            {
                points[k].x = r.x[j];
                points[k].y = r.y[j];
                points[k].z = r.z[j];
                /*min.x = (min.x > r.x[j]) ? r.x[j] : min.x;
                min.y = (min.y > r.y[j]) ? r.y[j] : min.y;
                min.z = (min.z > r.z[j]) ? r.z[j] : min.z;
                max.x = (max.x < r.x[j]) ? r.x[j] : max.x;
                max.y = (max.y < r.y[j]) ? r.y[j] : max.y;
                max.z = (max.z < r.z[j]) ? r.z[j] : max.z;*/
                GL.Vertex3(r.x[j], r.y[j], r.z[j]);
                GL.Vertex3(r.x[j + 1], r.y[j + 1], r.z[j + 1]);
                k++;   
            }
            Debug.Log("Drew it");
            GL.End();
            GL.PopMatrix();
            k = 0;
            
        }
        
    }
}
