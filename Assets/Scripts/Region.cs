﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class Region
{
    public int[] lineLengths;
    public int[] lineOffsets;
    public string regionName;
    public float[] x;
    public float[] y;
    public float[] z;
}