﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

public class MeshRegion : MonoBehaviour
{
    public string fileName;
    public string visit;
    public Transform RegionParent;
    public GameObject RegionObject;
    public Material lineMaterial;
    public Vector3 min = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    public Vector3 max = new Vector3(-Mathf.Infinity, -Mathf.Infinity, -Mathf.Infinity);
    public float Diameter;
    public Vector3 center;
    public int numberOfMeshes = 1;
    private Region r;
    void Start()
    {
        Debug.Log("Loading Region: " + fileName);
        r = readFile();
        instantiateRegion(r);
    }

    Region readFile()
    {
        Debug.Log(Application.streamingAssetsPath);
        string filePath = Application.streamingAssetsPath + "/" + fileName;
        return JsonUtility.FromJson<Region>(File.ReadAllText(filePath));      
    }

    /*void FixedUpdate()
    {

        if (runOnce)
        {
            runOnce = false;
            CreateMesh();
        }
    }

    /*public async void CreateMesh()
    {

        createMesh = Task.Run(() => instantiateRegion(r));
        await createMesh;
    }*/

void instantiateRegion(Region r)
    {
        Debug.Log("Read the file!!");
        var points = new Vector3[(r.x.Length)];
        int countSkip = 0;
        int countPoints = 1;
        int number = 1;
        List<Mesh> AllMeshes = new List<Mesh>(); //List of meshes
        points[0] = new Vector3(r.x[0], r.y[0], r.z[0]);
       // do
        //{
            List<int> Alltriangles = new List<int>(); //intialize a list of triangles
            List<Vector3> AllVertices = new List<Vector3>(); //intialize a list of vertices
                for (int i = number; i < 2; i++) //i<r.xlength
                {
                    points[i] = new Vector3(r.x[i], r.y[i], r.z[i]);
                    /*min.x = (min.x > r.x[i]) ? r.x[i] : min.x;
                    min.y = (min.y > r.y[i]) ? r.y[i] : min.y;
                    min.z = (min.z > r.z[i]) ? r.z[i] : min.z;
                    max.x = (max.x < r.x[i]) ? r.x[i] : max.x;
                    max.y = (max.y < r.y[i]) ? r.y[i] : max.y;
                    max.z = (max.z < r.z[i]) ? r.z[i] : max.z;*/
                    if (Array.Exists(r.lineOffsets, x => x == i)) //A lamda function which takes an input x puts it into x == i
                    {
                        countSkip++;
                        //delete the new object made
                        continue;
                    }
                    if(countPoints >= numberOfMeshes * 64000)
                    {
                        break;
                    }
                    countPoints++;
                    var slopePlane = points[i] - points[i - 1];
                    var pointOnPlane = points[i - 1];
                    var firstLine = Vector3.Cross(slopePlane, Vector3.right);
                    var secondLine = Vector3.Cross(firstLine, slopePlane);
                    var thirdLine = -firstLine;
                    var fourthLine = -secondLine;
                    var Vertices = new Vector3[8];
                    Vertices[0] = points[i - 1] + firstLine;
                    Vertices[1] = points[i - 1] + secondLine;
                    Vertices[2] = points[i - 1] + thirdLine;
                    Vertices[3] = points[i - 1] + fourthLine;
                    Vertices[4] = points[i] + firstLine;
                    Vertices[5] = points[i] + secondLine;
                    Vertices[6] = points[i] + thirdLine;
                    Vertices[7] = points[i] + fourthLine;
                    int[] triangles = { 
                                        3 + AllVertices.Count, 6 + AllVertices.Count, 7 + AllVertices.Count,
                                        2 + AllVertices.Count, 6 + AllVertices.Count, 3 + AllVertices.Count,
                                        0 + AllVertices.Count, 4 + AllVertices.Count, 5 + AllVertices.Count,
                                        0 + AllVertices.Count, 5 + AllVertices.Count, 1 + AllVertices.Count,
                                        1 + AllVertices.Count, 5 + AllVertices.Count, 6 + AllVertices.Count,
                                        1 + AllVertices.Count, 2 + AllVertices.Count, 6 + AllVertices.Count,
                                        0 + AllVertices.Count, 7 + AllVertices.Count, 4 + AllVertices.Count,
                                        0 + AllVertices.Count, 3 + AllVertices.Count, 7 + AllVertices.Count
                                      };
                    AllVertices.AddRange(Vertices);
                    Alltriangles.AddRange(triangles);

                }
            Mesh mesh = new Mesh(); //intialize a mesh
            mesh.vertices = AllVertices.ToArray();
            mesh.triangles = Alltriangles.ToArray();
            numberOfMeshes++;
            number = countPoints;
            AllMeshes.Add(mesh);
            Graphics.DrawMesh(mesh, Vector3.zero, Quaternion.identity, lineMaterial, 0);
            //this.GetComponent < MeshFilter >().mesh = mesh;
        //} while (countPoints <= r.x.Length);
    }
}
